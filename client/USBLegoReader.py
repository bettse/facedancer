# USBLegoReader.py
#
# Contains class definitions to implement a USB Lego Dimensions Portal.

from USB import *
from MAXUSBApp import *
from USBDevice import *
from USBConfiguration import *
from USBInterface import *
from USBEndpoint import *
from USBClass import *
from LegoDimensionsFirmware import *

class USBLegoReaderClass(USBClass):
    name = "USB Lego Reader class"
    SET_IDLE = 0x0A
    SET_PROTOCOL = 0x0B

    def setup_request_handlers(self):
        self.request_handlers = {
            USBLegoReaderClass.SET_IDLE: self.handle_set_idle,
            USBLegoReaderClass.SET_PROTOCOL: self.handle_set_protocol
        }

    def handle_set_idle(self, req):
      #print("SET_IDLE", req.value)
      #pin_control = self.interface.maxusb_app.read_register(MAXUSBApp.reg_pin_control)
      #self.interface.maxusb_app.write_register(MAXUSBApp.reg_pin_control, pin_control | MAXUSBApp.nak_ep3 | MAXUSBApp.nak_ep2 | MAXUSBApp.nak_ep0 )
      self.interface.configuration.device.send_control_message(bytes([req.value]))
      #self.interface.configuration.device.maxusb_app.stall_ep0()


    def handle_set_protocol(self, req):
      pass
      #HID_PROTO_BOOT = 0, HID_PROTO_REPORT = 1
      #print("SET_PROTOCOL", req.value)
      self.interface.configuration.device.send_control_message(bytes([req.value]))
      #self.interface.configuration.device.maxusb_app.stall_ep0()


class USBLegoReaderInterface(USBInterface):
    name = "USB Lego Dimensions Reader interface"

    #configuration_descriptor = b'\x09\x02\x29\x00\x01\x01\x00\x80\xFA'
    #interface_descriptor = b'\x09\x04\x00\x00\x02\x03\x00\x00\x00'
    hid_descriptor = b'\x09\x21\x00\x01\x00\x01\x22\x1D\x00'
    #interface_descriptor_in  = b'\x07\x05\x81\x03\x20\x00\x01'
    #interface_descriptor_out = b'\x07\x05\x01\x03\x20\x00\x01'
    report_descriptor = b'\x06\x00\xFF\x09\x01\xA1\x01\x19\x01\x29\x20\x15\x00\x26\xFF\x00\x75\x08\x95\x20\x81\x00\x19\x01\x29\x20\x91\x00\xC0'

    def __init__(self, maxusb_app, verbose=0, mitm=False):
        self.maxusb_app = maxusb_app
        descriptors = {
                USB.desc_type_hid    : self.hid_descriptor,
                USB.desc_type_report : self.handle_get_report_descriptor
        }

        #IN always refers to transfers to the host from a device
        self.inpoint = USBEndpoint(
                0x02,          # endpoint number
                USBEndpoint.direction_in,
                USBEndpoint.transfer_type_interrupt,
                USBEndpoint.sync_type_none,
                USBEndpoint.usage_type_data,
                0x2000,      # max packet size
                10,         # polling interval, see USB 2.0 spec Table 9-13
                self.handle_buffer_available    # handler function
        )

        #OUT always refers to transfers from the host to a device
        self.outpoint = USBEndpoint(
                0x01,          # endpoint number
                USBEndpoint.direction_out,
                USBEndpoint.transfer_type_interrupt,
                USBEndpoint.sync_type_none,
                USBEndpoint.usage_type_data,
                0x2000,      # max packet size
                10,         # polling interval, see USB 2.0 spec Table 9-13
                self.handle_data_available    # handler function
        )

        USBInterface.__init__(
                self,
                0,          # interface number
                0,          # alternate setting
                3,          # interface class # 3 = HID
                0,          # subclass
                0,          # protocol
                0,          # string index
                verbose,
                [ self.inpoint, self.outpoint ],
                descriptors
        )
        self.device_class = USBLegoReaderClass()
        self.device_class.set_interface(self)

        self.firmware = LegoDimensionsFirmware(mitm=mitm)

    def handle_buffer_available(self):
        response = self.firmware.data_in()
        if len(response) > 0:
          self.maxusb_app.send_on_endpoint(self.inpoint.number & 0x0F, response)

    def handle_data_available(self, data):
        response = self.firmware.data_out(data)
        if len(response) > 0:
          self.maxusb_app.send_on_endpoint(self.inpoint.number & 0x0F, response)

    def handle_get_report_descriptor(self, dIndex):
        print(self.name, "GET_REPORT_DESCRIPTOR")
        return bytearray([
          0x06, 0x00, 0xFF, #Usage page
          0x09, 0x01,       #Usage
          0xa1, 0x01,       #Collection
          0x19, 0x01, #usage min
          0x29, 0x20, #usage max
          0x15, 0x00, #Logical min
          0x26, 0xff, 0x00, #logical max
          0x75, 0x08,  #report size
          0x95, 0x20, #report count
          0x81, 0x00, #input
          0x19, 0x01, #usage min
          0x29, 0x20, #usage max
          0x91, 0x00, #output
          0xc0  #end collection
          ])



class USBLegoReaderDevice(USBDevice):
    name = "USB Lego Reader device"

    def __init__(self, maxusb_app, loglevel=0, mitm=False):
        verbose = int(5 - (loglevel/10))
        print(self.name, "verbosity set to", verbose)
        interface = USBLegoReaderInterface(maxusb_app, verbose=verbose, mitm=mitm)
        config = USBConfiguration(
                1,                # index
                None,             # string desc
                [ interface ]     # interfaces
        )

        USBDevice.__init__(
                self,
                maxusb_app,
                0,                      # device class
                0,                      # device subclass
                0,                      # protocol release number
                0x40,                   # max packet size for endpoint 0
                0x0e6f,                 # vendor id
                0x0241,                 # product id
                0x0100,                 # device revision
                "PDP LIMITED. ",        # manufacturer string
                "LEGO READER V2.10",    # product string
                "P.D.P.000000",         # serial number string
                [ config ],
                verbose=verbose
        )

