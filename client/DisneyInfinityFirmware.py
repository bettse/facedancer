from struct import *
from BurtleRandom import *
import hid
import os
import logging
import coloredlogs
from time import sleep

def bytes_as_hex(b, delim=" "):
    return delim.join(["%02x" % x for x in b])

class PortalCommands:
  Activate = 0x80
  SeedRand = 0x81
  NextRand = 0x83
  LightOn = 0x90
  LightFade = 0x92
  LightFlash = 0x93
  Light95 = 0x95
  Light96 = 0x96
  Presence = 0xA1
  ReadData = 0xA2
  WriteData = 0xA3
  ReadTag = 0xB4
  UnknownB5 = 0xB5
  Unknown95 = 0x95
  Unknown96 = 0x96

class Token:
  BLOCK_SIZE = 16
  BLOCK_COUNT = 20
  SIZE = BLOCK_SIZE * BLOCK_COUNT
  UID_LEN = 7

class PortalLights():
  def __init__(self, loglevel=logging.WARNING):
    self.logger = logging.getLogger("%s.%s" % (__name__, self.__class__.__name__))
    self.logger.setLevel(loglevel)
  def on(self, platform, r, g, b):
    self.logger.info("Light on %d: %02x%02x%02x" % (platform, r, g, b))
  def fade(self, platform, r, g, b, speed, alternations):
    self.logger.info("Light fade %d: %02x%02x%02x" % (platform, r, g, b))
    sleep(0.5)
  def flash(self, platform, r, g, b, wait_a, wait_b, alternations):
    self.logger.info("Light flash %d: %02x%02x%02x" % (platform, r, g, b))
    sleep(0.5)


class PortalData():
  PortalFolder = './portal'

  def __init__(self, loglevel=logging.INFO):
    self.logger = logging.getLogger("%s.%s" % (__name__, self.__class__.__name__))
    self.logger.setLevel(loglevel)
    self.currentUid = None
    self.update = None

  def files(self):
    return [f for f in os.listdir(self.PortalFolder) if Token.SIZE == os.path.getsize(os.path.join(self.PortalFolder, f))]

  #Files in a directory represent the tokens on the portal
  # high nibble is platform (led), low nibble is nfc index
  # hex = 1, left(player 1) = 2, right(player 2) = 3
  # Starting with support for 1 token
  def presence(self):
    response = bytes([])
    if self.currentUid:
      response = bytes([0x20, 0x09])
    self.logger.info("presence: %s" % bytes_as_hex(response))
    return response

  def checkUpdate(self):
    if len(self.files()) == 0:
      if self.currentUid:
        #Token left
        self.currentUid = None
        self.update = {'nfcIndex': 0, 'direction': 1}
    else:
      uidStr, ext = os.path.splitext(self.files()[0])
      uid = bytes.fromhex(uidStr)
      if uid != self.currentUid:
        #Token arrived
        self.currentUid = uid
        self.update = {'nfcIndex': 0, 'direction': 0}

  def tagid(self, nfcIndex):
    uid = self.currentUid
    if (uid):
      self.logger.info("Read tag uid (%s) on nfcIndex %d" % (bytes_as_hex(uid, delim=''), nfcIndex))
      return uid
    else:
      return bytes([])

  def nfcImage(self, nfcIndex):
    filename = "%s.bin" % bytes_as_hex(self.currentUid, delim='')
    tag = open("%s/%s" % (self.PortalFolder, filename), "rb+")
    return tag

  def read(self, nfcIndex, sector, block):
    self.logger.info("Read %x:%x from nfcIndex %u" % (sector, block, nfcIndex))
    start = (sector * 4 + block) * Token.BLOCK_SIZE # 4 blocks to a sector, 16 bytes to a block
    tag = self.nfcImage(nfcIndex)
    tag.seek(start)
    data = tag.read(Token.BLOCK_SIZE)
    tag.close()
    return data

  def write(self, nfcIndex, sector, block, data):
    self.logger.info("Write %x:%x to nfcIndex %u" % (sector, block, nfcIndex))
    start = (sector * 4 + block) * Token.BLOCK_SIZE # 4 blocks to a sector, 16 bytes to a block
    tag = self.nfcImage(nfcIndex)
    tag.seek(start)
    count = tag.write(data)
    tag.close()
    return count

class PortalAutenticator():
  def __init__(self, loglevel=logging.WARNING):
    self.logger = logging.getLogger("%s.%s" % (__name__, self.__class__.__name__))
    self.logger.setLevel(loglevel)

  def setSeed(self, scrambledSeed):
    seed = self.descramble(scrambledSeed)
    self.logger.info("Set seed %08x" % seed)
    self.burtle = BurtleRandom(seed)

  def getNext(self):
    value = self.burtle.value()
    self.logger.info("Get next: %08x" % value)
    return self.scramble(value)

  def descramble(self, scrambled):
    mask = 0x5517999cd855aa71
    clear = 0

    for i in range(64):
      if (mask & 1):
        clear = clear << 1
        clear |= (scrambled & 1)

      scrambled = scrambled >> 1
      mask = mask >> 1
    return clear

  def scramble(self, clear):
    mask = 0x8E55AA1B3999E8AA
    scrambled = 0

    for i in range(64):
      scrambled = scrambled << 1
      if (mask & 1):
        scrambled |= (clear & 1)
        clear = clear >> 1
      mask = mask >> 1
    return scrambled


class DisneyInfinityFirmware():
  name = "Disney Infinity Firmware"
  commandIndex = 0
  lengthIndex = 1
  contentStart = 2
  REPORT_LEN = 0x20

  def __init__(self, loglevel=logging.INFO, mitm = False):
    if mitm:
      self.h = hid.device()
      self.h.open(0x0E6F, 0x0129)

      loglevel = logging.DEBUG
      self.portal_logger = logging.getLogger('DisneyInfinityDevice')
      self.portal_logger.setLevel(loglevel)
    else:
      self.h = None

    self.logger = logging.getLogger("%s.%s" % (__name__, self.__class__.__name__))
    self.logger.setLevel(loglevel)
    self.host_logger = logging.getLogger('WiiU')
    self.host_logger.setLevel(loglevel)
    self.lights = PortalLights(logging.WARNING)
    self.data = PortalData(loglevel)
    self.authenticity = PortalAutenticator(logging.WARNING)
    #coloredlogs.install(level=loglevel, show_hostname=False)
    logging.basicConfig(level=loglevel, format='%(levelname)-8s %(name)-50s %(message)s')


    #TODO: Create methods on each delegate that returns a hash of commands it handles, allow direct delegation
    self.command_handlers = {
      PortalCommands.Activate : self.activate,
      PortalCommands.Presence: self.presence,
      PortalCommands.SeedRand: self.seed_rand,
      PortalCommands.NextRand: self.next_rand,
      PortalCommands.LightOn: self.light_on,
      PortalCommands.LightFade: self.light_fade,
      PortalCommands.LightFlash: self.light_flash,
      PortalCommands.ReadData: self.read_data,
      PortalCommands.WriteData: self.write_data,
      PortalCommands.ReadTag: self.read_tag,
      PortalCommands.UnknownB5: self.ignore_command,
      PortalCommands.Unknown95: self.ignore_command,
      PortalCommands.Unknown96: self.ignore_command,
    }

  def data_out(self, data):
    self.data.checkUpdate()
    data = data[:self.REPORT_LEN]
    self.host_logger.debug((bytes_as_hex(data)))
    literal, length, command, correlation = unpack('BBBB', data[:4])
    contents = data[4:4+length-2] #contents are length - 2 (command/correlation) bytes starting after correlation

    response = self.empty_response(correlation)
    handler = self.command_handlers.get(command, None)
    if handler:
      response = handler(correlation, contents)
    else:
      self.logger.warning("No handler for %x" % command)

    self.logger.debug(bytes_as_hex(response))

    if self.h:
      self.h.write(data)
      response = bytes(self.h.read(self.REPORT_LEN))
      self.portal_logger.debug((bytes_as_hex(response)))

    return response

  def data_in(self):
    if self.data.update:
      update = self.data.update
      self.data.update = None
      #AB LL 02 09 NF DD KK
      literal = 0xAB
      length = 4
      ledPlatform = 2
      nineByte = 9
      nfcIndex = update['nfcIndex']
      #DD: 0 = arriving, 1 = leaving
      direction = update['direction']
      response = pack('BBBBBB', literal, length, ledPlatform, nineByte, nfcIndex, direction)
      self.logger.info("nfcIndex %u is %s" % (nfcIndex, 'arriving' if direction == 0 else 'leaving'))
      self.logger.debug(bytes_as_hex(response))
      return self.add_padding(self.add_checksum(response))
    else:
      return bytes([])

  def empty_response(self, correlation):
    return self.format_response(correlation, b'')

  def raw_response(self, contents):
    padding = bytes([0x00] * (self.REPORT_LEN - len(contents)))
    return contents + padding

  def format_response(self, correlation, contents):
    literal = 0xAA
    length = len(contents) + 1 #1 for correlation
    response = pack('BBB', literal, length, correlation) + contents
    return self.add_padding(self.add_checksum(response))

  def add_checksum(self, contents):
    return contents + pack('B', sum(contents) & 0xFF)

  def add_padding(self, contents):
    padding = bytes([0x00] * (self.REPORT_LEN - len(contents)))
    return contents + padding

  def ignore_command(self, correlation, contents):
    return self.empty_response(correlation)

  def activate(self, correlation, contents):
    return self.raw_response(b'\xaa\x15' + bytes(correlation) + b'\x00\x0f\x01\x00\x03\x02\x09\x09\x43\x08\x55\x40\x37\x30\x53\x42\x48\xe1\x54\x73\xb2')

  def seed_rand(self, correlation, contents):
    scrambled = unpack('>Q', contents)[0]
    self.authenticity.setSeed(scrambled)
    return self.empty_response(correlation)

  def next_rand(self, correlation, contents):
    value = self.authenticity.getNext()
    content = pack('>Q', value)
    return self.format_response(correlation, content)

  def light_on(self, correlation, contents):
    platform, r, g, b = unpack('BBBB', contents)
    self.lights.on(platform, r, g, b)
    return self.empty_response(correlation)

  def light_fade(self, correlation, contents):
    platform, r, g, b, speed, alternations = unpack('BBBBBB', contents)
    self.lights.fade(platform, r, g, b, speed, alternations)
    return self.empty_response(correlation)

  def light_flash(self, correlation, contents):
    platform, r, g, b, wait_a, wait_b, alternations= unpack('BBBBBBB', contents)
    self.lights.flash(platform, r, g, b, wait_a, wait_b, alternations)
    return self.empty_response(correlation)

  def presence(self, correlation, contents):
    contents = self.data.presence()
    return self.format_response(correlation, contents)

  def read_tag(self, correlation, contents):
    nfcIndex = unpack('B', contents)[0]
    tagid = self.data.tagid(nfcIndex)
    if tagid:
      return self.format_response(correlation, pack('B', nfcIndex) + tagid)
    else:
      return self.empty_response(correlation)

  def read_data(self, correlation, contents):
    nfcIndex, sector, block = unpack('BBB', contents)
    data = self.data.read(nfcIndex, sector, block)
    if data:
      return self.format_response(correlation, pack('B', nfcIndex) + data)
    else:
      return self.empty_response(correlation)

  def write_data(self, correlation, contents):
    nfcIndex, sector, block = unpack('BBB', contents[:3])
    new_data = contents[3:]
    count = self.data.write(nfcIndex, sector, block, new_data)
    if count:
      return self.format_response(correlation, pack('B', nfcIndex))
    else:
      return self.empty_response(correlation)

