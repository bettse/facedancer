from struct import *
from BurtleRandom import *
import hid
import os
import logging
import coloredlogs
from time import sleep
import facedancer_pb2

def bytes_as_hex(b, delim=" "):
    return delim.join(["%02x" % x for x in b])

class ReaderCommands:
  Activate = 0xB0
  SeedRand = 0xB1
  NextRand = 0xB3
  LightOn = 0xC0
  LightFade = 0xC2
  LightC6 = 0xC6
  Presence = 0xD0
  DataRead = 0xD2
  DataWrite = 0xD3
  D4 = 0xD4

class NTAG213:
  SIZE = 180
  PAGE_SIZE = 4

class LegoReader():
  ARRIVING = 0
  DEPARTING = 1
  CENTER = 1
  LEFT = 2
  RIGHT = 3


class ReaderLights():
  def __init__(self, loglevel=logging.INFO):
    self.logger = logging.getLogger("%s.%s" % (__name__, self.__class__.__name__))
    self.logger.setLevel(loglevel)
  def on(self, platform, r, g, b):
    self.logger.info("Light on %d: %02x%02x%02x" % (platform, r, g, b))
  def fade(self, platform, r, g, b, speed, alternations):
    self.logger.info("Light fade %d: %02x%02x%02x" % (platform, r, g, b))
    sleep(0.5)
  def flash(self, platform, r, g, b, wait_a, wait_b, alternations):
    self.logger.info("Light flash %d: %02x%02x%02x" % (platform, r, g, b))
    sleep(0.5)


class LegoDimensionsFirmware():
  name = "Lego Dimensions Firmware"
  commandIndex = 0
  lengthIndex = 1
  contentStart = 2
  REPORT_LEN = 0x20

  def __init__(self, loglevel=logging.DEBUG, mitm = False):
    self.h = hid.device()
    self.h.open(0x0E6F, 0x0241)
    self.h.set_nonblocking(True)
    self.read_timeout_ms = 250

    self.reader_logger= logging.getLogger('ToyPad')
    self.reader_logger.setLevel(loglevel)

    self.logger = logging.getLogger("%s.%s" % (__name__, self.__class__.__name__))
    self.logger.setLevel(loglevel)
    self.host_logger = logging.getLogger('WiiU')
    self.host_logger.setLevel(loglevel)
    logging.basicConfig(level=loglevel, format='%(levelname)-8s %(name)-50s %(message)s')
    self.lights = ReaderLights(loglevel)
    self.data = ReaderData(loglevel)
    self.activated = False

    self.command_handlers = {
      ReaderCommands.Activate : self.activate,
      ReaderCommands.SeedRand: self.seed_rand,
      ReaderCommands.NextRand: self.next_rand,
      ReaderCommands.LightOn: self.light_on,
      ReaderCommands.LightFade: self.light_fade,
      ReaderCommands.LightC6: self.ignore_command,
      ReaderCommands.DataRead: self.data_read,
      ReaderCommands.DataWrite: self.data_write,
      ReaderCommands.Presence: self.presence,
      ReaderCommands.D4: self.d4,
    }


  def data_out(self, data):
    data = data[:self.REPORT_LEN]
    self.host_logger.debug((bytes_as_hex(data)))
    message = facedancer_pb2.HIDMessage()
    message.message = data
    cereal = message.SerializeToString()

    literal, length, command, correlation = unpack('BBBB', data[:4])
    contents = data[4:4+length-2] #contents are length - 2 (command/correlation) bytes starting after correlation

    response = bytes([])

    MITM = True

    if MITM:
      self.h.write(data)
    else:
      self.activated |= (command == ReaderCommands.Activate)
      handler = self.command_handlers.get(command, None)
      if handler:
        response = handler(correlation, contents)
      else:
        self.logger.warning("No handler for %s" % (bytes_as_hex(data)))

    return response

  def data_in(self):
    response = bytes([])
    MITM = True

    if MITM:
      response = bytes(self.h.read(self.REPORT_LEN, self.read_timeout_ms))
      if len(response) > 0:
        self.reader_logger.debug((bytes_as_hex(response)))
    elif self.activated:
      contents = self.data.update()
      if (len(contents) > 0) :
        response = self.format_update(contents)
        self.logger.debug((bytes_as_hex(response)))

    return response

  def empty_response(self, correlation):
    return self.format_response(correlation, b'')

  def format_response(self, correlation, contents):
    literal = 0x55
    length = len(contents) + 1 #1 for correlation
    response = pack('BBB', literal, length, correlation) + contents
    return self.add_padding(self.add_checksum(response))

  def format_update(self, contents):
    literal = 0x56
    length = len(contents)
    response = pack('BB', literal, length) + contents
    return self.add_padding(self.add_checksum(response))

  def add_checksum(self, contents):
    return contents + pack('B', sum(contents) & 0xFF)

  def add_padding(self, contents):
    padding = bytes([0x00] * (self.REPORT_LEN - len(contents)))
    return contents + padding

  def ignore_command(self, correlation, contents):
    return self.empty_response(correlation)

  def activate(self, correlation, contents):
    return self.format_response(correlation, bytes.fromhex("002f020102020402f500194553dc2deeae4e98c904023034"))

  def seed_rand(self, correlation, contents):
    header = bytes.fromhex("55 0a b1")
    reader_command = self.add_checksum(header + pack('B', correlation) + contents)
    self.h.write(reader_command)
    reader_response = bytes(self.h.read(self.REPORT_LEN))
    return self.format_response(correlation, reader_response[3:11])

  def next_rand(self, correlation, contents):
    header = bytes.fromhex("55 0a b3")
    reader_command = self.add_checksum(header + pack('B', correlation) + contents)
    self.h.write(reader_command)
    reader_response = bytes(self.h.read(self.REPORT_LEN))
    return self.format_response(correlation, reader_response[3:11])

  def light_on(self, correlation, contents):
    platform, r, g, b = unpack('BBBB', contents)
    self.lights.on(platform, r, g, b)
    return self.empty_response(correlation)

  def light_fade(self, correlation, contents):
    platform, r, g, b, speed, alternations = unpack('BBBBBB', contents)
    self.lights.fade(platform, r, g, b, speed, alternations)
    return self.empty_response(correlation)

  def light_flash(self, correlation, contents):
    platform, r, g, b, wait_a, wait_b, alternations= unpack('BBBBBBB', contents)
    self.lights.flash(platform, r, g, b, wait_a, wait_b, alternations)
    return self.empty_response(correlation)

  def data_read(self, correlation, contents):
    nfcIndex, pageNumber = unpack('BB', contents)
    pages = self.data.read(nfcIndex, pageNumber)
    return self.format_response(correlation, pack('B', nfcIndex) + pages)

  def data_write(self, correlation, contents):
    #TODO: hook up with self.data.write()
    return self.empty_response(correlation)

  def presence(self, correlation, contents):
    status = bytes([])
    for nfxIndex, item in enumerate(self.tokens):
      platform = item['platform']
      status += pack('BB', platform * 16 + nfcIndex, 00)
    return self.format_response(correlation, status)

  def d4(self, correlation, contents):
    header = bytes.fromhex("55 0a d4")
    reader_command = self.add_checksum(header + pack('B', correlation) + contents)
    self.h.write(reader_command)
    reader_response = bytes(self.h.read(self.REPORT_LEN))
    self.reader_logger.debug((bytes_as_hex(reader_response)))
    return self.format_response(correlation, reader_response[3:11])

class ReaderData():
  ReaderFolder = './toypad'

  def __init__(self, loglevel=logging.INFO):
    self.logger = logging.getLogger("%s.%s" % (__name__, self.__class__.__name__))
    self.logger.setLevel(loglevel)
    self.tokens = []

  def files(self):
    return [f for f in os.listdir(self.ReaderFolder) if NTAG213.SIZE == os.path.getsize(os.path.join(self.ReaderFolder, f))]

  def uidOfFile(self, f):
    uidStr, ext = os.path.splitext(f)
    uid = bytes.fromhex(uidStr)
    return uid

  def update(self):
    contents = bytes([])
    platform = LegoReader.LEFT

    for nfcIndex, uid in enumerate(self.tokens):
      direction = LegoReader.DEPARTING
      if uid not in [self.uidOfFile(f) for f in self.files()]:
        self.logger.info("Removing %s" % bytes_as_hex(uid, delim=''))
        self.tokens.remove(uid)
        contents = pack('BBBB', platform, 00, nfcIndex, direction) + uid
        break

    for f in self.files():
      direction = LegoReader.ARRIVING
      uid = self.uidOfFile(f)
      if uid not in self.tokens:
        self.logger.info("Adding %s" % bytes_as_hex(uid, delim=''))
        self.tokens.append(uid)
        nfcIndex = self.tokens.index(uid)
        contents = pack('BBBB', platform, 00, nfcIndex, direction) + uid
        break

    #self.logger.debug("Token list: %s | File list: %s" % (self.tokens, self.files()))
    return contents

  def nfcImage(self, nfcIndex):
    files = self.files()
    filename = files[nfcIndex]
    tag = open("%s/%s" % (self.ReaderFolder, filename), "rb+")
    return tag

  def read(self, nfcIndex, page):
    start = page * NTAG213.PAGE_SIZE
    tag = self.nfcImage(nfcIndex)
    tag.seek(start)
    data = tag.read(4 * NTAG213.PAGE_SIZE) #always reads 4 pages
    self.logger.info("Read %x from nfcIndex %u [%s]" % (page, nfcIndex, bytes_as_hex(data)))
    tag.close()
    return data

  def write(self, nfcIndex, page, data):
    self.logger.info("Write %x to nfcIndex %u" % (page, nfcIndex))
    start = page * NTAG.PAGE_SIZE
    tag = self.nfcImage(nfcIndex)
    tag.seek(start)
    count = tag.write(data)
    tag.close()
    return count


