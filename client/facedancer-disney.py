#!/usr/bin/env python3
#
# facedancer-portal.py

from optparse import OptionParser
from Facedancer import *
from MAXUSBApp import *
from USBDisneyInfinityPortal import *

def main(opts, args):
  sp = GoodFETSerialPort(port="/dev/tty.usbserial-A902UFTU")
  fd = Facedancer(sp, verbose=0)
  u = MAXUSBApp(fd, verbose=0)

  d = USBDisneyInfinityPortalDevice(u, loglevel=opts.loglevel, mitm=opts.mitm)

  d.connect()

  try:
      d.run()
  # SIGINT raises KeyboardInterrupt
  except KeyboardInterrupt:
      d.disconnect()

if __name__ == '__main__':
    # Setup the command line arguments.
    optp = OptionParser()

    # Output verbosity options.
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-i', '--info', help='set logging to INFO',
                    action='store_const', dest='loglevel',
                    const=logging.INFO, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                    action='store_const', dest='loglevel',
                    const=5, default=logging.INFO)

    optp.add_option('-m', '--mitm', help='Enable Man in the middle',
                    action='store_const', dest='mitm',
                    const=True, default=False)


    opts, args = optp.parse_args()

    main(opts, args)

