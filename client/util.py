# util.py
#
# Random helpful functions.

from termcolor import colored, cprint

def bytes_as_hex(b, delim=" "):
    return delim.join(["%02x" % x for x in b])

def warning(text):
    cprint(text, 'magenta')

def log(text):
    cprint(text, 'blue')

def info(text):
    cprint(text, 'yellow')

def debug(text):
    cprint(text, 'white')

def error(text):
    cprint(text, 'red')

